import * as MSG from '../constants/message';
import {MIN_PASS_LENGHT} from '../constants/constant';
export const validateFirstName = value => {
  return (value.trim() === "") ? MSG.ER_ENTER_FIRST_NAME:"";
};

export const validateLastName = value => {
    return (value.trim() === "") ? MSG.ER_ENTER_LAST_NAME : "";
}

export const validateUserName = value => {
  return (value.trim() === "") ? MSG.ER_ENTER_USER_NAME : "";
}
export const validateEmailAddress = value => {

  let email = value.trim();
  if(email === "")
  {
    return MSG.ER_ENTER_EMAIL;
  }
  else
  {
    const reg = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    if (reg.test(value) === true)
    {
      return "";
    }
    else
    {
      return MSG.ER_ENTER_VALID_EMAIL_ADDRESS;
    }
  }
};

export const validatePassword = (value) => {
 
  let password = value.trim();
  if(password === "")
  {
    return MSG.ER_ENTER_PASSWORD;
  }
  else
  {
      if (password.length < MIN_PASS_LENGHT)
      {
        return MSG.ER_PASSWORD_LENGTH;
      }
      else
      {
        return "";
      }
    
  }
};


export const validateTermsAndCondition = value => {
  return (value) ? "":MSG.ER_SELECT_TERM_AND_CONDITION; 
};
