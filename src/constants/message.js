import * as CONSTANT from './constant';
export const ER_ENTER_FIRST_NAME = 'First name is required';
export const ER_ENTER_LAST_NAME = 'Last name is required';
export const ER_ENTER_USER_NAME = 'Username is required';
export const ER_ENTER_EMAIL = 'Email is required';
export const ER_ENTER_VALID_EMAIL_ADDRESS = 'Please enter valid email address';
export const ER_ENTER_PASSWORD = 'Password is required';
export const ER_PASSWORD_LENGTH = `Minimum ${CONSTANT.MIN_PASS_LENGHT} character is required`;
export const ER_SELECT_TERM_AND_CONDITION = "Please accept Terms and Conditions to continue";