import React, { useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { SignupForm } from "../../components/atoms";
import 'react-tabs/style/react-tabs.css';
import './SignupStyle.css';


const Signup = () => {
  const [tabIndex, setTabIndex] = useState(0);
  return (
    <div className="d-flex flex-column signup-outer">
      <div className="align-items-center justify-content-center g-0 min-vh-100 row">
        <div className="signup">
          <div className="card form-container">
            <div className="card-body signup-form">
              <Tabs selectedIndex={tabIndex} onSelect={index => setTabIndex(index)}>
                <div className="tab-menu">
                  <TabList>
                      <Tab>FAN SIGNUP</Tab>
                      <Tab>TALENT SIGNUP</Tab>
                  </TabList>
                </div>
                <TabPanel>
                  <SignupForm type={'fan'} headingText={'Create Your Fan Account'} />
                </TabPanel>
                <TabPanel>
                  <SignupForm type={'talent'} headingText={'Create Your Talent Account'} />
                </TabPanel>
              </Tabs>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  );
}
export default Signup;