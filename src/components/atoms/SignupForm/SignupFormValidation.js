
import * as Validation from '../../../utility/validation';

const SignupFormValidation =(values)=> {
  let errors = {};
  errors.first_name = Validation.validateFirstName(values.first_name);
  errors.last_name = Validation.validateLastName(values.last_name);
  errors.username = Validation.validateUserName(values.username);
  errors.email = Validation.validateEmailAddress(values.email);
  errors.password = Validation.validatePassword(values.password);
  errors.is_agree = Validation.validateTermsAndCondition(values.is_agree);

  errors = Object.entries(errors).reduce((a,[k,v]) => (v ? (a[k]=v, a) : a), {})
  return errors;
}
export default SignupFormValidation;