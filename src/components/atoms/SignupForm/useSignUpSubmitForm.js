import { useEffect, useState } from "react";
import SignupFormValidation from "./SignupFormValidation";
import { serverCall } from "../../../services/request";

const initialState = {
  first_name: "",
  last_name: "",
  username: "",
  email: "",
  password: "",
  is_agree: false,
};
const useSignUpSubmitForm = (type) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isValidate, setIsValidate] = useState(false);
  const [apiResponseMsg, setApiResponseMsg] = useState({});
  const [inputValue, setInputValue] = useState(initialState);
  const [errors, setErrors] = useState({});

  const removeAlertMessage = () => {
    setApiResponseMsg({});
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue((prev) => ({
        ...prev,
        [name]: name === "is_agree" ? !inputValue['is_agree']:value,
    }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    setErrors(SignupFormValidation(inputValue));
    setIsValidate(true);
  };

  const signUpFormSave = async () => {
    setIsLoading(true);
    try {
      let postData = inputValue;
      delete postData.is_agree;
      let url = `http://wren.in:3200/api/sign-up/${type}`;
      let response = await serverCall(url, "POST", postData);
      if (
        response.statusText !== undefined &&
        response.statusText == "Created"
      ) {
        setInputValue(initialState);
        setApiResponseMsg({
          messageType: "success",
          headingTitle: "Success!",
          bodyMessage: "Your account has been created successfully!",
        });
      } else {
        setApiResponseMsg({
          messageType: "error",
          headingTitle: "Error!",
          bodyMessage: "Something went wrong!",
        });
      }
    } catch (error) {
      setApiResponseMsg({
        messageType: "error",
        headingTitle: error.title,
        bodyMessage: error.message,
      });
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isValidate === true) {
      signUpFormSave();
      setIsValidate(false);
    }
  }, [errors]);

  return {
    handleChange,
    handleFormSubmit,
    removeAlertMessage,
    errors,
    inputValue,
    isLoading,
    apiResponseMsg,
  };
};

export default useSignUpSubmitForm;