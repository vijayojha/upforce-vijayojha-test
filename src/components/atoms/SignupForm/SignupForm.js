import React from "react";
import PropTypes from "prop-types";
import { Loader, Alert } from "../../molecules";
import "./SignupFormStyle.css";
import useSignUpSubmitForm from "./useSignUpSubmitForm";

const SignupForm = (props) => {
  const { type, headingText } = props;
  const {
    handleChange,
    handleFormSubmit,
    inputValue,
    errors,
    isLoading,
    apiResponseMsg,
    removeAlertMessage,
  } = useSignUpSubmitForm(type);
  const { first_name, last_name, username, email, password, is_agree } =
    inputValue;

  return (
    <div>
      <h2>{headingText}</h2>
      {Object.keys(apiResponseMsg).length > 0 && (
        <Alert
          messageType={apiResponseMsg.messageType}
          headingTitle={apiResponseMsg.headingTitle}
          bodyMessage={apiResponseMsg.bodyMessage}
          onClose={() => {
            removeAlertMessage();
          }}
        />
      )}

      <form autoComplete="off" onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label htmlFor="input-field">First name*</label>
          <input
            type="text"
            value={first_name}
            name="first_name"
            className="form-control"
            placeholder="First name"
            onChange={handleChange}
          />
          {errors.first_name !== undefined && errors.first_name && (
            <p className="text-danger">{errors.first_name}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="input-field">Last name*</label>
          <input
            type="text"
            value={last_name}
            name="last_name"
            className="form-control"
            placeholder="Last name"
            onChange={handleChange}
          />
          {errors.last_name !== undefined && errors.last_name && (
            <p className="text-danger">{errors.last_name}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="input-field">Username*</label>
          <input
            type="text"
            value={username}
            name="username"
            className="form-control"
            placeholder="Username"
            onChange={handleChange}
          />
          {errors.username !== undefined && errors.username && (
            <p className="text-danger">{errors.username}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="input-field">Email*</label>
          <input
            type="text"
            value={email}
            name="email"
            className="form-control"
            placeholder="Email"
            onChange={handleChange}
          />
          {errors.email !== undefined && errors.email && (
            <p className="text-danger">{errors.email}</p>
          )}
        </div>

        <div className="form-group">
          <label htmlFor="input-field">Password*</label>
          <input
            type="password"
            value={password}
            name="password"
            className="form-control"
            placeholder="Password"
            onChange={handleChange}
          />
          {errors.password !== undefined && errors.password && (
            <p className="text-danger">{errors.password}</p>
          )}
        </div>

        <div className="form-group form-check">
          <div className="check-block">
            <input
              type="checkbox"
              className="form-check-input"
              name="is_agree"
              id="is_agree"
              onChange={handleChange}
              checked={is_agree}
            />
            <label class="form-check-label" for="is_agree">
              I agree to the{" "}
              <a href="javascript:void(0);">Terms and Conditions</a>
            </label>
          </div>
          {errors.is_agree !== undefined && errors.is_agree && (
            <p className="text-danger">{errors.is_agree}</p>
          )}
        </div>

        <div className="button-block">
          <button type="submit" className="btn btn-success">
            Sign Up
          </button>
        </div>
      </form>
      <Loader loading={isLoading} />
    </div>
  );
};
SignupForm.propTypes = {
  type: PropTypes.string.isRequired,
  headingText: PropTypes.string.isRequired,
};

export default SignupForm;
