import React from 'react';
import PropTypes from 'prop-types';

const Alert = (props) => {
    const {messageType,headingTitle,bodyMessage,onClose} = props;
    let alertClass = '';
    if(messageType === "success"){
      alertClass="alert-success";
    }else if(messageType === "error"){
      alertClass="alert-danger";
    }
    return (
      <div className={`alert ${alertClass}`} role="alert">
            <h4 className="alert-heading">{headingTitle}</h4>
            <p>{bodyMessage}</p>
            <button type="button" className="close" onClick={onClose}>
               <span>&times;</span>
            </button>
          </div>
    )
};


Alert.defaultProps = {
  messageType: "",
  headingTitle: "",
  bodyMessage: ""
}

Alert.propTypes = {
  messageType: PropTypes.string,
  headingTitle: PropTypes.string,
  bodyMessage: PropTypes.string,
  onClose:PropTypes.func
}

export default Alert;