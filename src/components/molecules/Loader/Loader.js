import React from 'react';
import PropTypes from 'prop-types';
import Spinner from './spinner.svg'
import './LoaderStyles.css';

const Loader = (props) => {
    if (props.loading) {
        return (
          <div className="loading-background">
             <img src={Spinner} className="img-style"/>
           </div> 
        )
      }
    return null 
};

Loader.defaultProps = {
    loading: false,
}
  
Loader.propTypes = {
    loading: PropTypes.bool,
}

export default Loader;
