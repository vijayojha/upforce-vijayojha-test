import axios from "axios";
export const serverCall = (url, method, postData = {}, isRaw = true) => {
  return new Promise(function (success, failure) {
    let headers = { "Content-Type": "application/json" };
    let mainPostData = {};
    if (isRaw) {
      mainPostData = JSON.stringify(postData);
    } else {
      let allKeys = Object.keys(postData);
      let allValues = Object.values(postData);
      const formData = new FormData();
      allKeys.map((currentKey, index) => {
        formData.append(currentKey, allValues[index]);
      });
      mainPostData = formData;
    }
    axios.defaults.timeout = 1000 * 10;
    axios({
      method: method,
      data: mainPostData,
      url: url,
      headers: headers,
    })
      .then((response) => {
        success(response);
      })
      .catch((error) => {
        failure({ message: error.message, title: "Server Error!" });
      });
  });
};
